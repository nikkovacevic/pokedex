FROM node:16.14.0
WORKDIR /pokedex
COPY . .
RUN npm ci
EXPOSE 3000
CMD ["npx", "serve", "build"]