import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Header from '../components/Header';

it('prikaze naslov aplikacije', () => {
	render(<Header/>);
	expect(screen.getByText('Pokedex')).toBeTruthy();
});

it('klik na lupo', async () => {
	render(<Header/>);
	await userEvent.click(screen.getByAltText('search icon'));
	expect(screen.getByRole('button')).toBeTruthy()
})