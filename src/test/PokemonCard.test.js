import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PokemonCard from '../components/PokemonCard';

const obj = {
	id: 9,
	name: 'blastoise',
	pkmn: {
		name: 'blastoise',
		url: 'https://pokeapi.co/api/v2/pokemon/9/'
	},
	url: 'https://pokeapi.co/api/v2/pokemon/9/'
}

it('renderanje pokemon carda', () => {
	render(<PokemonCard {...obj} id={obj.id} pkmn={obj}/>)
	expect(screen.getByText('BLASTOISE')).toBeTruthy();
})

it('hover pokemon carda', async () => {
	render(<PokemonCard {...obj} id={obj.id} pkmn={obj}/>)
	await userEvent.click(screen.getByText('BLASTOISE'));
	expect(screen.getByText('BLASTOISE')).toBeTruthy();
})