import './App.scss';
import VsiPokemoni from './components/VsiPokemoni';
import PokemonPage from './components/PokemonPage';
import { BrowserRouter as Router, Route, Routes, } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

function App() {
	return (

		<QueryClientProvider client={queryClient}>
			<Router>
				<Routes>
					<Route exact path="/" element={<VsiPokemoni/>}/>
					<Route path="/pokemon/:id" element={<PokemonPage/>}/>
				</Routes>
			</Router>
		</QueryClientProvider>
	);
}

export default App;
