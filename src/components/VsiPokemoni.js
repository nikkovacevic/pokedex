import React from 'react'
import PokemonCard from './PokemonCard';
import Header from './Header';
import { useQuery } from 'react-query';

const URL = 'https://pokeapi.co/api/v2/pokemon/?limit=151';

export default function VsiPokemoni() {

    const { isLoading, error, data } = useQuery('pokemonData', () =>
        fetch(URL).then(res => res.json())
    )

    if (isLoading) return <div>Loading...</div>

    if (error) return <div>Error... {error.message}</div>

    return ( 
        <>
            <Header/>
        
            <div className="pokecards"> 
                {data?.results?.map((pokemon, index) => {
                        return <PokemonCard key={index} {...pokemon} id={index+1} pkmn={pokemon} />;
                })}
            </div>  
        </>
    );
};
